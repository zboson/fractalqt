QT = core gui widgets

HEADERS += renderthread.h gui.h vectorclass.h vectorclass_extra.h

SOURCES += \
    main.cpp \
    renderthread.cpp \
    gui.cpp \
    kernel.cpp

QMAKE_CXXFLAGS_RELEASE -= -O
QMAKE_CXXFLAGS_RELEASE -= -O1
QMAKE_CXXFLAGS_RELEASE -= -O2

#msvc:QMAKE_CXXFLAGS_RELEASE += /O2 /openmp /arch:AVX
#gcc:QMAKE_CXXFLAGS_RELEASE += -O3 -mavx -fopenmp
#gcc:QMAKE_CXXFLAGS_RELEASE += -O3 -march=native -fopenmp -D_GLIBCXX_PARALLEL

msvc {
  QMAKE_CXXFLAGS += -openmp -arch:AVX -D "_CRT_SECURE_NO_WARNINGS"
  QMAKE_CXXFLAGS_RELEASE *= -O2
  QMAKE_LFLAGS += kernel_gcc_nofma.obj
}

gcc {
  #QMAKE_CXXFLAGS += -fopenmp -mavx -fabi-version=0
  #QMAKE_CXXFLAGS += -fopenmp -march=native -fabi-version=0
  QMAKE_CXXFLAGS += -fopenmp -mavx -fabi-version=0 -ffast-math
  QMAKE_LFLAGS += -fopenmp
  QMAKE_CXXFLAGS_RELEASE *= -O3
}

win32-g++ {
  QMAKE_CXXFLAGS += -fopenmp -mavx -fabi-version=0
  QMAKE_LFLAGS += -fopenmp
  QMAKE_CXXFLAGS_RELEASE *= -O3
}
