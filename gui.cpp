#include "gui.h"
#include <QPainter>
#include <QTimer>
#include <QTextStream>
#include <QGraphicsView>
#include <QAction>
#include <QMenuBar>
#include <QVBoxLayout>
#include <math.h>


#include "renderthread.h"
#include "instrset.h"

extern "C" void kernel_AVX2(const float8& a, const int gid, const Fvalues *fvalues, int8& out);
extern "C" void kernel_AVX(const float8& a, const int gid, const Fvalues *fvalues, int8& out);
extern "C" void kernel_SSE41(const float8& a, const int gid, const Fvalues *fvalues, int8& out);
extern "C" void kernel_SSE2(const float8& a, const int gid, const Fvalues *fvalues, int8 &out);

void (*kernel)(const float8& a, const int gid, const Fvalues *fvalues, int8 &out);

/*
  float (x87)
  float4 (SSE)
  float8 (AVX)
  double (x87)
  double2 (SSE)
  double4 (AVX)
  fma (haswell)
  //fixed piont 128-bit
  f128 (SSE)
  f128x2 (AVX)

  OpenCL float8 (CPU - AMD/Intel)
  OpenCL double4 (CPU - AMD/Intel)
  OpenCL GPU Nvidia/AMD

 */

/*
full screen
grab/free mouse
set window size
set render size
 */

/*
 color maps
 */

void Fractal::init() {
	int iset = instrset_detect();
	if(iset==8) {
	    kernel = kernel_AVX2;
	    printf("FMA\n");
	}
	else if(iset==7) {
		kernel = kernel_AVX;
		printf("AVX\n");	
	}
	else if(iset==5) {
		kernel = kernel_SSE41;
		printf("SSE4.1\n");
	}
	else {
	    kernel = kernel_SSE2;
	    printf("SSE2\n");
	}
}

Fractal::Fractal(uint _width, uint _height, QWidget *parent) {
    init();
    resize_sw = false;
    type = 0;
    vstate=2;
    nthreads = omp_get_num_procs();
    //fvalues.minx = -2.5;
    //fvalues.maxx =  1.5;
    //fvalues.miny = -2.0;
    //fvalues.maxy =  2.0;
    //fvalues.centerx = -0.5;
    fvalues.centerx = -0.7;
    fvalues.centery = 0.0;
    scrollStep = 10.0;
    //fvalues.deltax = (fvalues.maxx-fvalues.minx)/width();
    //fvalues.deltay = (fvalues.maxy-fvalues.miny)/height();
    //fvalues.deltax = 3.5;
    //fvalues.deltay = 2.0;
    fvalues.deltax = 3.5  * exp(-0.3);
    fvalues.deltay = 2.0  * exp(-0.3);

    //fvalues.deltay   = -(4/3.0f)*2.7f*height/width;
    fvalues.minx    = fvalues.centerx - fvalues.deltax * 0.5;
    fvalues.miny    = fvalues.centery - fvalues.deltay * 0.5;
    fvalues.maxx    = fvalues.centerx + fvalues.deltax * 0.5;
    fvalues.maxy    = fvalues.centery + fvalues.deltay * 0.5;


    scale = 1.0;
    zoom = 1.0;
    zoom_speed = 1.0;

    QDesktopWidget desktop;

    uint desktopHeight=desktop.geometry().height();
    uint desktopWidth=desktop.geometry().width();
    //qDebug("width heigth %d %d", desktopWidth, desktopHeight);
    move(100, 100);

    setWindowTitle("SIMD fractal");
    //setAutoFillBackground(false);
    //setBackgroundRole((QPalette::ColorRole)Qt::red );



    //setWindowState(Qt::WindowFullScreen);
    cnt = 1;
    timerId = startTimer(1);
    //qDebug("width heigth %d %d", this->width(), this->height());
    //width = _width;
    //height = _height;
   // buffer = new int[desktopWidth*desktopHeight];
   //for(uint i=0; i<(desktopWidth*desktopHeight); i++) {
   //     buffer[i] = 0;
   // }
    time_tmp = 0;
    //QPoint glob = mapToGlobal(QPoint(width()/2,height()/2));
    //QCursor::setPos(glob);
    //lastPosition = QPoint(width()/2,height()/2);
    //setCursor(Qt::BlankCursor);
    //setCursor(QPixmap(1,1));
    //setCursor(Qt::CrossCursor);

    //QWSServer::setCursorVisible( false );
    //QRect asdf = QApplication::desktop()->screenGeometry();

    setMouseTracking(true);
    captureMouse = false;

    QWidget *widget = new QWidget;
    setCentralWidget(widget);

    openAction = new QAction(tr("&FullScreen"), this);
    saveAction = new QAction(tr("&Save"), this);
    exitAction = new QAction(tr("E&xit"), this);

    //connect(openAction, SIGNAL(triggered()), this, SLOT(open()));
    //connect(saveAction, SIGNAL(triggered()), this, SLOT(save()));
    connect(exitAction, SIGNAL(triggered()), this, SLOT(quit()));

    fileMenu = menuBar()->addMenu(tr("&File"));
    fileMenu->addAction(openAction);
    fileMenu->addAction(saveAction);
    fileMenu->addSeparator();
    fileMenu->addAction(exitAction);

    textEdit = new QTextEdit;
    QVBoxLayout *layout = new QVBoxLayout;
    quitButton = new QPushButton(tr("Quit"));
    connect(quitButton, SIGNAL(clicked()), this, SLOT(quit()));
    //layout->addWidget(textEdit);
    layout->addWidget(quitButton);
    //widget->setLayout(layout);
    thread.init(desktopWidth, desktopHeight);
    connect(&thread,
            SIGNAL(renderedImage(int*, double)), this, SLOT(updatePixmap(int*, double)));
    buffer = NULL;
    //render_next = true;
    resize(_width, _height);
    show();
}

void Fractal::updatePixmap(int* buffer, double dtime) {
    this->buffer = buffer;
    this->kernel_time = dtime;
    //render_next = true;
    //update();
    draw_time = omp_get_wtime();
    repaint();
    draw_time = omp_get_wtime() - draw_time;
}

void Fractal::quit() {
    //qDebug("asdf");
    //QApplication::quit();
    qApp->quit();
    //exit(0);
}

void Fractal::render_buffer() {
    fvalues.width = width();
    fvalues.height = height();

    fvalues.radius2 = 4*4;
    fvalues.maxiter = 250;
    fvalues.zoom = 1.0;

    fvalues.deltax = scale*(fvalues.maxx-fvalues.minx)/width();
    fvalues.deltay = scale*(fvalues.maxy-fvalues.miny)/height();
    //fvalues.deltax*=exp(-0.3);
    //fvalues.deltay*=exp(-0.3);
    //printf("width %d, height %d, deltax %f, deltay %f\n",
    //       fvalues.width, fvalues.height, fvalues.deltax, fvalues.deltay);
}

void Fractal::paintEvent(QPaintEvent *e)
{
  Q_UNUSED(e);
    if(resize_sw) {
        resize_sw = false;
        return;
    }
  QPainter painter(this);

  QImage image(width(),height(), QImage::Format_RGB32);
  //qDebug("width %d, height %d\n", image.width(), image.height());
  if(buffer != NULL)
    memcpy(image.bits(), buffer, 4*width()*height());

  //qDebug("cnt %d", cnt);

  QPixmap pix1 = QPixmap::fromImage(image);
  painter.drawPixmap(0,0, width(), height(), pix1);
}

void Fractal::resizeEvent(QResizeEvent* e)
{
    resize_sw = true;
    //fvalues.deltax = (fvalues.maxx-fvalues.minx)/width();
    //fvalues.deltay = (fvalues.maxy-fvalues.miny)/height();
    update_window();
    //render_buffer();
    //thread.render(fvalues);
    //update();
      //m_pPalette->setBrush(QPalette::Background,QBrush(m_pPixmap->scaled(width(),height())));
      //setPalette(*m_pPalette);
}

void Fractal::update_window() {
    render_buffer();

    //if(thread.isFinished());
    //if(!thread.isRunning());
    char asdf[256];
    //if(render_next) {
    int vsizeaf[3] = {1,4,8};
    int vsizead[3] = {1,2,4};
    int vsize;
    if(type==0) {
        vsize = vsizeaf[vstate];
    }
    else {
        vsize = vsizead[vstate];
    }
    if(thread.restart) {
      render_next = false;
      thread.render(fvalues, vsize, type, nthreads);

    //thread.wait();
      const char* type_str[] = {"float", "double"};
      sprintf(asdf, "kernel_time us %.2f, draw_time us %.2f, FPS %.2f, scale %f, zoom_speed %f, %s%d, nthreads %d",
              1000*kernel_time, 1000*draw_time, 1.0f/kernel_time, scale, zoom_speed, type_str[type],vsize, nthreads);
      this->setWindowTitle(asdf);
    }
}
void Fractal::timerEvent(QTimerEvent *e)
{
  Q_UNUSED(e);

  cnt += 1;
  if(zoom>1) {
    scale*=zoom*zoom_speed;
  }
  else {
    scale*=zoom/zoom_speed;
  }

  if(captureMouse) {
      QPoint position = QCursor::pos();
      float dx = float(position.x() - lastPosition.x()) / width();
      float dy = float(position.y() - lastPosition.y()) / height();
      QPoint glob = mapToGlobal(QPoint(width()/2,height()/2));
      QCursor::setPos(glob);
      //lastPosition = QPoint(width()/2,height()/2);
      lastPosition = glob;

      fvalues.centerx += 500*dx*fvalues.deltax;
      fvalues.centery += 500*dy*fvalues.deltay;
  }
  update_window();
}

void Fractal::keyPressEvent(QKeyEvent *event) {
    switch (event->key()) {
    case Qt::Key_Equal:
        zoom_speed +=0.0001;
        break;
    case Qt::Key_Minus:
        zoom_speed -=0.0001;
        break;
    case Qt::Key_0:
        zoom_speed = 1.0;
        break;
    case Qt::Key_T:
        type++;
        if(type>1) type = 0;
        break;
    case Qt::Key_V:
        vstate+=1;
        if(vstate>2) vstate = 0;
        break;
    case Qt::Key_P:
        nthreads+=1;
        if(nthreads>omp_get_num_procs()) nthreads = 1;
        break;
    case Qt::Key_Left:
        fvalues.centerx -= scrollStep*fvalues.deltax;
        break;
    case Qt::Key_Right:
        fvalues.centerx += scrollStep*fvalues.deltax;
        break;
    case Qt::Key_Down:       
        fvalues.centery += scrollStep*fvalues.deltay;
        break;
    case Qt::Key_Up:
        fvalues.centery -= scrollStep*fvalues.deltay;
        break;
    case Qt::Key_F12:
        //qDebug("F12");
        if(isFullScreen()) {
            showNormal();
        }
        else {
            setWindowState(Qt::WindowFullScreen);
            captureMouse = true;
            setCursor(Qt::BlankCursor);
            QPoint glob = mapToGlobal(QPoint(width()/2,height()/2));
            QCursor::setPos(glob);
            lastPosition = glob;
        }
        break;
    case Qt::Key_CapsLock:
        captureMouse = captureMouse ? false : true;
        if(captureMouse) {
            setCursor(Qt::BlankCursor);
            QPoint glob = mapToGlobal(QPoint(width()/2,height()/2));
            QCursor::setPos(glob);
            lastPosition = glob;
        }
        else {
            setCursor(Qt::BitmapCursor);
        }
        break;

    case Qt::Key_Escape:
        exit(0);
        break;
    default:
        QWidget::keyPressEvent(event);
    }
}

void Fractal::mousePressEvent(QMouseEvent *event)
{

    if (event->button() == Qt::LeftButton) {
        //zoom = 0.98;
    }
    else if (event->button() == Qt::RightButton) {
        //zoom = 1.02;
        //qDebug("right moust button");
    }
        //lastDragPos = event->pos();

}

void Fractal::mouseMoveEvent(QMouseEvent *event)
{
    /*
    float dx = float(event->x() - lastPosition.x()) / width();
    float dy = float(event->y() - lastPosition.y()) / height();
    QPoint glob = mapToGlobal(QPoint(width()/2,height()/2));
    QCursor::setPos(glob);
    lastPosition = QPoint(width()/2,height()/2);

    fvalues.centerx += 500*dx*fvalues.deltax;
    fvalues.centery += 500*dy*fvalues.deltay;
    qDebug("dx %f, dy %f", dx, dy);
    */
    //QGLWidget::mouseMoveEvent(event);
    //if (event->buttons() & Qt::LeftButton) {
        //QPoint point = event->pos();
        //qDebug("1 x,y %d %d", point.x(), point.y());
        //qDebug("2 x,y %d %d", event->x(), event->y());

        //pixmapOffset += event->pos() - lastDragPos;
        //lastDragPos = event->pos();
        //update();
    //}
/*
    QPoint position = QCursor::pos() - pos();
    qDebug("asdf %f %f", pos().x(), pos().y());
    QPoint glob = mapToGlobal(QPoint(width()/2,height()/2));
    QCursor::setPos(glob);

    //float dx = (1.0*lastPosition.x()-0.5f*width())/width();
    //float dy = (1.0*lastPosition.y()-0.5f*height())/height();

    float dx = position.x() - width()/2;
    float dy = position.y() - height()/2;

    //float dx = position.x() - lastPosition.x();
    //float dy = position.y() - lastPosition.y();

    //fvalues.centerx = dx*fvalues.deltax;
    //fvalues.centery = dy*fvalues.deltay;

    if(position != lastPosition){
        fvalues.centerx += dx*fvalues.deltax;
        fvalues.centery += dy*fvalues.deltay;
        //qDebug("%d %d", position.x()-lastPosition.x(), position.y()-lastPosition.y());
        //qDebug("%d %d", position.x(), position.y());
        lastPosition = position;
        qDebug("mouse %d %d, %f %f", lastPosition.x(), lastPosition.y(), dx, dy);

    }
    QWidget::mouseMoveEvent(event);
*/
}

void Fractal::wheelEvent(QWheelEvent *event)
{
    int numDegrees = event->delta() / 8;
    double numSteps = numDegrees / 15.0f;
    zoom_speed += numSteps/100;
    //if(zoom_speed <1) zoom_speed = 1;
    qDebug("zoom_sped %f, numSteps %f", zoom_speed, numSteps);
}

void Fractal::mouseReleaseEvent(QMouseEvent *event)
{

    if (event->button() == Qt::LeftButton) {
       zoom = 1.0;
        //qDebug("left moust button");
    }
    else if (event->button() == Qt::RightButton) {
       zoom = 1.0;
        //qDebug("right moust button");
    }
    /*
    if (event->button() == Qt::LeftButton) {

        pixmapOffset += event->pos() - lastDragPos;
        lastDragPos = QPoint();

        int deltaX = (width() - pixmap.width()) / 2 - pixmapOffset.x();
        int deltaY = (height() - pixmap.height()) / 2 - pixmapOffset.y();
        scroll(deltaX, deltaY);

    }
    */
}

