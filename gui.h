#pragma once

#include <QWidget>
#include <QKeyEvent>
#include <QDesktopWidget>
#include <QLabel>
#include <QMenu>
#include <QLineEdit>
#include <QPushButton>
#include <QTextEdit>
#include <QtGui>
#include <QMainWindow>
#include <omp.h>

#include "renderthread.h"

class Fractal : public QMainWindow
{
  Q_OBJECT

  public:
    Fractal(uint width, uint height, QWidget *parent = 0);
    void init();
    void render_buffer();
    void update_window();

  protected:
    void paintEvent(QPaintEvent *event);
    void resizeEvent(QResizeEvent* event);
    void timerEvent(QTimerEvent *event);
    void keyPressEvent(QKeyEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void wheelEvent(QWheelEvent *event);

  private:
    bool render_next;
    bool resize_sw;
    int cnt;
    int vstate;
    int type;
    int nthreads;

    double scale;
    double zoom;
    qreal opacity;
    int timerId;
    //QPixmap pix1;
    //QImage image;
    int *buffer;
    RenderThread thread;
    //uint width;
    //uint height;
    double time;
    double kernel_time;
    double draw_time;
    double time_tmp;
    double scrollStep;
    QPoint lastPosition;
    bool captureMouse;
    double zoom_speed;

    QPushButton *quitButton;
    QTextEdit *textEdit;

    QAction *openAction;
    QAction *saveAction;
    QAction *exitAction;

    QMenu *fileMenu;

    Fvalues fvalues;

  private slots:
    void quit();
    void updatePixmap(int* buffer, double kernel_time);
    /*

    QLabel* label1;
    QMenu *bar;
    QMenu *bar1;
    QLineEdit *line;
    QAction* showAct;
    QAction* hideAct;
    QAction* exitAct;
    QAction* copy;
    QAction* cut;
    QAction* paste;
    */
/*
    struct {
        int width;
        int height;
        int maxiter;
        double radius2;
        double centerx;
        double centery;
        double deltax;
        double deltay;
        //double minx;
        //double miny;
        //double maxx;
        //double maxy;
        double zoom;
     } fvalues;
*/

};
