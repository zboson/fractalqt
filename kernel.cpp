#include "stdio.h"
#include "vectorclass.h"
#include "vectormath_common.h"

// Define function name depending on which instruction set we compile for
#if   INSTRSET == 2                    // SSE2
#define FUNCNAME kernel_SSE2
#elif INSTRSET == 5                    // SSE4.1
#define FUNCNAME kernel_SSE41
#elif INSTRSET == 7                    // AVX
#define FUNCNAME kernel_AVX
#elif INSTRSET == 8                    // AVX2
#define FUNCNAME kernel_AVX2
#elif INSTRSET == 9                    // AVX512
#define FUNCNAME kernel_AVX512
#endif

typedef Vec8f float8;
typedef Vec8fb bool8f;
typedef Vec8i int8;

typedef float ftype;

typedef struct {
   int width;
   int height;
   int maxiter;
   double radius2;
   double centerx;
   double centery;
   double deltax;
   double deltay;
   double minx;
   double miny;
   double maxx;
   double maxy;
   double zoom;
} Fvalues;

extern "C" void FUNCNAME(const float8& a, const int gid, const Fvalues *fvalues, int8& out) {
    //#if defined (__FMA__)
    //printf("%d\n", __FMA__);
    //#endif
    // iterates z = z + c until |z| >= radius2 or maxiter is reached,
    //return result of vecn pixels
    const int ix = gid%fvalues->width, iy = gid/fvalues->width;

    float8 x0 = 1.0*ix + a;
    x0 = fvalues->centerx + (x0 - fvalues->width/2)*fvalues->deltax;
    float8 y0 = fvalues->centery + (iy - fvalues->height/2)*fvalues->deltay;

    float8 x = x0;
    float8 y = y0;
    float8 vf = 0;
    bool8f maskf = 0;
   
    for(int n = 0; n<fvalues->maxiter; n++) {
        float8 zn2_temp = x*x + y*y;
        //float8 zn2_temp = _mm256_fmadd_ps(x,x, y*y);
        maskf = zn2_temp <= fvalues->radius2;
        if(to_bits(maskf)==0) break;
        vf += Vec8f(maskf) & (Vec8f)1;
        //incf(vf, maskf);
        float8 xtemp = x;
        //_mm256_fmadd_ps
        //x = _mm256_fmadd_ps(x, x, x0) - y*y;
        //x = _mm256_fmadd_ps(x, x, x0-y*y);
        //x = mul_add(x,x,x0) - y*y;
        x = mul_sub(x,x,mul_sub(y,y,x0));
        //x = x*x - mul_sub(y,y,x0);
        //x = x*x - y*y + x0;
        //y = _mm256_fmadd_ps(2*xtemp,y,y0);
        y = mul_add(2*xtemp,y,y0);
        //y = 2*xtemp*y + y0;
    }
    out = truncate_to_int(vf);
}

