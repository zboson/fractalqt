#include "vectorclass_extra.h"

typedef struct {
   int width;
   int height;
   int maxiter;
   double radius2;
   double centerx;
   double centery;
   double deltax;
   double deltay;
   double minx;
   double miny;
   double maxx;
   double maxy;
   double zoom;

} Fvalues;

template <typename floatn, typename intn>
intn getColorn(intn it, int maxIt, int *mColormap, const int COLORMAP_SIZE) {
  floatn moo = (to_float_new(it)*COLORMAP_SIZE)/maxIt;
  intn index = truncate_to_int_new(moo);
  //intn index = (it*COLORMAP_SIZE)/maxIt;
  index = select(index<0, 0, index);
  index = select(index>=COLORMAP_SIZE, COLORMAP_SIZE-1, index);

  //intn out = lookup<2048>(index, mColormap);
  intn out = get_mapedcolors(index, mColormap);
  //out = select(it<0, 0xFFFFFFFF, out);
  //out = select(it<1, 0, out);
  out = select(it>=maxIt, 0, out);

  return out;
}

template <typename floatn, typename intn>
inline void kernel_Mandelbrot_vecn(const floatn& a, const int gid, const Fvalues fvalues, int* pixels, int* mColormap) {
  //  typedef float8 floatn;
  //  typedef int8 intn;
    //typedef typename Tupple<floatn>::intn intn;
    // iterates z = z + c until |z| >= radius2 or maxiter is reached,
    //return result of vecn pixels
    const int ix = gid%fvalues.width, iy = gid/fvalues.width;

    floatn x0 = 1.0*ix + a;
    x0 = (fvalues.maxx - fvalues.minx) * x0/(double)fvalues.width + fvalues.minx;
    floatn y0 = (fvalues.maxy - fvalues.miny) * iy/fvalues.height + fvalues.miny;

    floatn x = x0;
    floatn y = y0;
    intn vn = 0;
    intn mask = 0;
    floatn zn2 = 0;

    for(int n = 0; n<fvalues.maxiter; n++) {
        floatn zn2_temp = x*x + y*y;
        //zn2 = zn2_temp;
        zn2 = select(mask, zn2_temp, zn2);
        mask = zn2_temp <= fvalues.radius2;
        if(cnt_mask(mask)==0) break;
        inc(vn, mask);
        floatn xtemp =x;
        x = x*x - y*y + x0;
        y = 2*xtemp*y + y0;
    }
    //vn = select(vn == fvalues.maxiter, 0, vn);
    intn out = getColorn<floatn, intn>(vn, fvalues.maxiter, mColormap, 2048);

    //intn out = get_color_vecn(vn, fvalues.maxiter, sqrt(fvalues.radius2), sqrt(zn2));
    //store(vn, &pixels[gid]);
    store(out, &pixels[gid]);
}
