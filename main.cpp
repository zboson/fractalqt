#include "gui.h"
#include <QApplication>

int main(int argc, char *argv[])
{
  QApplication app(argc, argv);

  const uint width = 1024;
  const uint height = 768;
  Fractal window(width, height);

  //window.showFullScreen();
  //window.resize(width, height);

  //QApplication::desktop()->screenGeometry();
  //QDesktopWidget widget;
  //QRect mainScreenSize = window.availableGeometry(widget.primaryScreen());
  return app.exec();
  //QDesktopWidget desktop;

  //int desktopHeight=desktop.geometry().height();
  //int desktopWidth=desktop.geometry().width();
  //this->resize(desktopWidth,desktopHeight);
}
