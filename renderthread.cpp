#include "renderthread.h"
#include "vectorclass.h"
#include <QtWidgets>

#include <math.h>
#include <omp.h>
#include <x86intrin.h>

extern void (*kernel)(const float8& a, const int gid, const Fvalues *fvalues, int8& out);

RenderThread::RenderThread(QObject *parent)
    : QThread(parent)
{
    restart = true;
    abort = false;
}

void RenderThread::init(uint width, uint height) {
    //qDebug("qwerty %d %d", width, height);
    buffer = new int[width*height];
    //buffer = (int*)_mm_malloc(sizeof(int)*width*height, 64);
    for(uint i=0; i<(width*height); i++) {
        buffer[i] = 0;
    }
    mColormap = (int*)_mm_malloc(sizeof(int)*2048, 32);
    initColormap(0, 2048);
}

RenderThread::~RenderThread()
{
    mutex.lock();
    abort = true;
    condition.wakeOne();
    mutex.unlock();
    wait();
}

void RenderThread::render(Fvalues fvalues, int vsize, int type, int nthreads)
{

    QMutexLocker locker(&mutex);
   // qDebug("%d %d", fvalues.width, fvalues.height);
    this->fvalues = fvalues;
    this->type = type;
    this->vsize = vsize;
    this->nthreads = nthreads;
    if (!isRunning()) {
        //qDebug("asdf asdf %d", restart);
        start(LowPriority);
    } else {
        restart = true;
        condition.wakeOne();
    }
}

void RenderThread::run_kernel(Fvalues *fvalues, int gid) {
    double centerX = fvalues->centerx;
    double centerY = fvalues->centery;
    double deltaX = fvalues->deltax;
    double deltaY = fvalues->deltay;
    int width = fvalues->width;
    int height = fvalues->height;

    const int y = gid/width;
    const int x = gid%width;
    //qDebug("x %d, y %d", x, y);
    //qDebug("ASDF %d %d %f %f %f %f", width, height, centerX, centerY, deltaX, deltaY);
    //qDebug("maxit %d, radius %f", fvalues->maxiter, fvalues->radius2);

    const int min = 0;
    const int max = 200;

    const double Cx = centerX + (x-width/2)*deltaX;
    const double Cy = centerY + (y-height/2)*deltaY;

    double Zx=0.0;
    double Zy=0.0;
    double Zx2=Zx*Zx;
    double Zy2=Zy*Zy;
    int iter;
    for (iter=0;iter<fvalues->maxiter && ((Zx2+Zy2)<fvalues->radius2); iter++)
    {
        Zy=2*Zx*Zy + Cy;
        Zx=Zx2-Zy2 + Cx;
        Zx2=Zx*Zx;
        Zy2=Zy*Zy;
    };

    rgba_t p;
    hsv_to_rgb(iter, min, max, &p);
    int* color = (int*)&p;
    if(iter == fvalues->maxiter) *color = 0xff000000;
    //qDebug("iter %d, min %d, max %d, i %d, j %d, color %x", iter, min, max, x,y,*color);
    buffer[y*fvalues->width + x] = *color;
}

void RenderThread::run()
{
    float f1 = 0.0f;
    float4 f4 = float4(0.0f, 1.0f, 2.0f, 3.0f);
    float8 f8 = float8(0.0f, 1.0f, 2.0f, 3.0f, 4.0f, 5.0f, 6.0f, 7.0f);

    double d1 = 0.0;
    double2 d2 = double2(0.0, 1.0);
    double4 d4 = double4(0.0, 1.0, 2.0, 3.0);

    //int vsize = 8;
    //int type = 0;
    //vsize = 1;
    double dtime = omp_get_wtime();
    restart = false;
    int* pixels = buffer;
    //#pragma omp parallel for schedule(dynamic)
    //for(int gid=0; gid<(fvalues.width*fvalues.height/vsize); gid++) {
    //    kernel_Mandelbrot_vecn<float8, int8>(f8, vsize*gid, &fvalues, buffer, mColormap);
    //    //run_kernel(&fvalues, gid);
    //}

    //vsize = 8;
    omp_set_num_threads(nthreads);
    #pragma omp parallel for schedule(dynamic)
    for(int i = 0; i<(fvalues.width*fvalues.height)/vsize; i++) {
        if(type == 0) {
            int8 v8, out;
            switch(vsize) {
            case 8:
	      //kernel(f8, vsize*i, &fvalues, v8);
	      //out = getColorn<float8, int8, bool8f>(v8, fvalues.maxiter, mColormap, 2048);
	      //store(out, &pixels[vsize*i]);
                kernel_Mandelbrot_vecn<float8, int8, bool8f>(f8, vsize*i, &fvalues, pixels, mColormap);
                //post_process(num_pixels, fvalues.maxiter, pixels);
                break;
            case 4:
                kernel_Mandelbrot_vecn<float4, int4, bool4f>(f4, vsize*i, &fvalues, pixels, mColormap);
                break;
            case 1:
                kernel_Mandelbrot_vecn<float1, int1, bool1f>(f1, vsize*i, &fvalues, pixels, mColormap);
                break;
            }
        }
        else {
            switch(vsize) {
            case 4:
                kernel_Mandelbrot_vecn<double4, long4, bool4d>(d4, vsize*i, &fvalues, pixels, mColormap);
                break;
            case 2:
                kernel_Mandelbrot_vecn<double2, long2, bool2d>(d2, vsize*i, &fvalues, pixels, mColormap);
                break;
            case 1:
                kernel_Mandelbrot_vecn<double1, long1, bool1d>(d1, vsize*i, &fvalues, pixels, mColormap);
                break;
            }
        }
        //kernel(i, fvalues, pixels);
    }


    dtime = omp_get_wtime() - dtime;
    emit renderedImage(buffer, dtime);

    mutex.lock();
    restart = true;
    mutex.unlock();

    //mutex.lock();
    //if (!restart)
    //    condition.wait(&mutex);
    //restart = false;
    //mutex.unlock();

}

void RenderThread::hsv_to_rgb(int hue, int min, int max, rgba_t *p)
{
    int invert = 0;
    int color_rotate = 0;
    int saturation = 1;
    if (min == max) max = min + 1;
    if (invert) hue = max - (hue - min);
    if (!saturation) {
        p->r = p->g = p->b = 255 * (max - hue) / (max - min);
        return;
    }
    double h = fmod(color_rotate + 1e-4 + 4.0 * (hue - min) / (max - min), 6);
#	define VAL 255
    double c = VAL * saturation;
    double X = c * (1 - fabs(fmod(h, 2) - 1));

    p->r = p->g = p->b = 0;
    p->a = 255;
    switch((int)h) {
    case 0: p->r = c; p->g = X; return;
    case 1:	p->r = X; p->g = c; return;
    case 2: p->g = c; p->b = X; return;
    case 3: p->g = X; p->b = c; return;
    case 4: p->r = X; p->b = c; return;
    default:p->r = c; p->b = X;
    }
}

void RenderThread::initColormap(int c, const int COLORMAP_SIZE) {
  int n = COLORMAP_SIZE;
  for (int i=0; i<n; i++) {
    int h,s,v;
    switch (c) {
    default:
    case 0:
        h = (360*i)/n;
        s = 255;
        v = 255;
        break;
    case 1:
        h = 240 + (180*i)/n; // blue-yellow
        s = 255;
        v = 155+(100*i)/n;
        break;
    case 2:
        h = 0;
        s = 0;
        v = (255*i)/n;
        break;

    }
    //mColormap[i] = hsv(h%360,s,v);
    mColormap[i] = hsvToRgb(h,s,v);
  }
}

int RenderThread::hsvToRgb(double h, double s, double v) {
  h /= 360.0;
  s /= 255.0;
  v /= 255.0;
  double r, g, b;

  if (s<=0.0001) {
    r=g=b=v;
  }
  else {
    int i = floor(h * 6);
    double f = h * 6 - i;
    double p = v * (1 - s);
    double q = v * (1 - f * s);
    double t = v * (1 - (1 - f) * s);

    switch(i % 6){
        case 0: r = v, g = t, b = p; break;
        case 1: r = q, g = v, b = p; break;
        case 2: r = p, g = v, b = t; break;
        case 3: r = p, g = q, b = v; break;
        case 4: r = t, g = p, b = v; break;
        case 5: r = v, g = p, b = q; break;
    }
  }

  int rr = (int)(r*255);
  int gg = (int)(g*255);
  int bb = (int)(b*255);
  //int rr = (int)(r*255); if (rr>255) rr=255; if(rr<0) rr = 0;
  //int gg = (int)(g*255); if (gg>255) gg=255; if(gg<0) gg = 0;
  //int bb = (int)(b*255); if (bb>255) bb=255; if(bb<0) gg = 0;
  //return gg;
  return ((rr<<16) | (gg<<8) | bb );

}

template <typename floatn, typename intn, typename boolnf>
intn RenderThread::getColorn(intn it, int maxIt, int *colormap, const int COLORMAP_SIZE) {
  floatn moo = (to_float_new(it)*COLORMAP_SIZE)/maxIt;
  intn index = truncate_to_int_new(moo);
  //intn index = (it*COLORMAP_SIZE)/maxIt;
  index = select(index<0, 0, index);
  index = select(index>=COLORMAP_SIZE, COLORMAP_SIZE-1, index);

  //intn out = lookup<2048>(index, mColormap);
  intn out = get_mapedcolors(index, colormap);
  //out = select(it<0, 0xFFFFFFFF, out);
  //out = select(it<1, 0, out);
  out = select(it>=maxIt, 0, out);

  return out;
}

template <typename floatn, typename intn, typename boolnf>
inline void RenderThread::kernel_Mandelbrot_vecn(const floatn& a, const int gid, const Fvalues *fvalues, int* pixels, int* colormap) {
    // iterates z = z + c until |z| >= radius2 or maxiter is reached,
    //return result of vecn pixels
    const int ix = gid%fvalues->width, iy = gid/fvalues->width;

    floatn x0 = 1.0*ix + a;
    x0 = fvalues->centerx + (x0 - fvalues->width/2)*fvalues->deltax;
    floatn y0 = fvalues->centery + (iy - fvalues->height/2)*fvalues->deltay;

    floatn x = x0;
    floatn y = y0;
    floatn vf = 0;
    boolnf maskf = 0;

    for(int n = 0; n<fvalues->maxiter; n++) {
        floatn zn2_temp = x*x + y*y;
        maskf = zn2_temp <= fvalues->radius2;
        if(cnt_mask(maskf)==0) break;
        incf(vf, maskf);
        floatn xtemp = x;
        x = x*x - y*y + x0;
        y = 2*xtemp*y + y0;
    }

    intn vn = truncate_to_int_new(vf);
    intn out = getColorn<floatn, intn, boolnf>(vn, fvalues->maxiter, mColormap, 2048);
    store(out, &pixels[gid]);
}
