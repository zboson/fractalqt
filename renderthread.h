#ifndef RENDERTHREAD_H
#define RENDERTHREAD_H

#include <QMutex>
#include <QSize>
#include <QThread>
#include <QWaitCondition>

#include "vectorclass_extra.h"

typedef struct {unsigned char b, g, r, a;} rgba_t;

typedef struct {
   int width;
   int height;
   int maxiter;
   double radius2;
   double centerx;
   double centery;
   double deltax;
   double deltay;
   double minx;
   double miny;
   double maxx;
   double maxy;
   double zoom;
} Fvalues;

QT_BEGIN_NAMESPACE
class QImage;
QT_END_NAMESPACE

//! [0]
class RenderThread : public QThread
{
    Q_OBJECT

public:
    RenderThread(QObject *parent = 0);
    ~RenderThread();

    void init(uint width, uint height);
    void render(Fvalues fvalues, int vsize, int type, int nthreads);
    void run_kernel(Fvalues *fvalues, int gid);
    void hsv_to_rgb(int hue, int min, int max, rgba_t *p);
    int hsvToRgb(double h, double s, double v);
    void initColormap(int c, const int COLORMAP_SIZE);
    template <typename floatn, typename intn, typename boolnf> inline intn getColorn(intn it, int maxIt, int *colormap, const int COLORMAP_SIZE);
    template <typename floatn, typename intn, typename boolnf> inline void kernel_Mandelbrot_vecn(const floatn& a, const int gid, const Fvalues *fvalues, int* pixels, int* colormap);
      //  typedef float8 floatn;

    bool restart;
    int *mColormap;
    int vsize;
    int type;
    int nthreads;


signals:
    void renderedImage(int* buffer, double dtime);

protected:
    void run();
    void run2();

private:
    uint rgbFromWaveLength(double wave);

    QMutex mutex;
    QWaitCondition condition;
    Fvalues fvalues;
    int *buffer;
    //QSize resultSize;
    bool abort;
};
//! [0]

#endif // RENDERTHREAD_H
