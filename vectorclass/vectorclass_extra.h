#ifndef VECTORCLASSEXTRA_H
#define VECTORCLASSEXTRA_H 1
#include <stdio.h>
#include "vectorclass.h" 
#ifdef VECTORCLASS_H
	typedef Vec8f float8;
	typedef Vec8i int8;
    typedef Vec8fb bool8f;

	typedef Vec4f float4;
	typedef Vec4i int4;
    typedef Vec4fb bool4f;

	typedef Vec2d double2;
	typedef Vec2q long2;
    typedef Vec2db bool2d;

	typedef Vec4d double4;
	typedef Vec4q long4;
    typedef Vec4db bool4d;


	typedef float float1;
	typedef double double1;
	typedef int int1;
	typedef int64_t long1;
    typedef int bool1f;
    typedef int64_t bool1d;

#endif

template <typename F> struct Tupple {

};

template<> struct Tupple<float> {
    typedef int1 intn;
    typedef int1 boolnf;
	enum { fsize = 1 };
};

template<> struct Tupple<float4> {
	typedef int4 intn;
    typedef bool4f boolnf;
	enum { fsize = 4 };
};

template<> struct Tupple<float8> {
	typedef int8 intn;
    typedef bool8f boolnf;
	enum { fsize = 8 };
};

template<> struct Tupple<double> {
    typedef long1 intn;
    typedef bool1d boolnf;
	enum { fsize = 1 };
};

template<> struct Tupple<double2> {
	typedef long2 intn;
    typedef bool2d boolnf;
	enum { fsize = 2 };
};
template<> struct Tupple<double4> {
	typedef long4 intn;
    typedef bool4d boolnf;
	enum { fsize = 4 };
};

template<> struct Tupple<int> {
	typedef float floatn;
	enum { fsize = 1 };
};

template<> struct Tupple<int4> {
	typedef float4 floatn;
    typedef bool4f boolnf;
	enum { fsize = 4 };
};

template<> struct Tupple<int8> {
	typedef float8 floatn;
    typedef bool8f boolnf;
	enum { fsize = 8 };
};

template<> struct Tupple<int64_t> {
	typedef double floatn;
    typedef long1 boolnf;
	enum { fsize = 1 };
};

template<> struct Tupple<long2> {
	typedef double2 floatn;
    typedef int1 boolnf;
	enum { fsize = 2 };
};

template<> struct Tupple<long4> {
	typedef double4 floatn;
    typedef bool4f boolnf;
	enum { fsize = 4 };
};

inline int cnt_mask(const int mask) {
	return mask;
}

inline int64_t cnt_mask(const int64_t mask) {
	return mask;
}

inline int cnt_mask(const int4 &mask) {
	return _mm_movemask_epi8(mask);
}

inline int cnt_mask(const int8 &mask) {
	return _mm_movemask_epi8(mask.get_low()) + _mm_movemask_epi8(mask.get_high());
}

inline int cnt_mask(const long4 &mask) {
	return _mm_movemask_epi8(mask.get_low()) + _mm_movemask_epi8(mask.get_high());
}

inline int cnt_mask(const long2 &mask) {
	return _mm_movemask_epi8(mask);
}

inline void store(const int4 &v, int* x) {
	v.store(x);
}

inline void store(const int8 &v, int* x) {
	v.store(x);
}

inline void store(const int v, int* x) {
	*x = v;
}

inline void store(const long4 &v, int* x) {
	int4 out = compress(v, 0).get_low();
	out.store(x);
	//v.store(x);
}

inline void store(const long2 &v, int* x) {
	int4 out = compress(v, 0);
	//out.store(x);
	int x1 = out[0];
	int x2 = out[1];
	x[0] = x1;
	x[1] = x2;
	//v.store(x);
}

inline void store(const int64_t v, int* x) {
	*x = (int)v;
}

inline int select(const int v, const int a, const int b) {
	return v ? a : b;
}

inline float select(const int v, const float a, const float b) {
	return v ? a : b;
}

inline double select(const int v, const double a, const double b) {
	return v ? a : b;
}

inline void inc(int &x, const int mask) {
	x++;
}

inline void inc(int64_t &x, const int64_t mask) {
	x++;
}

inline void inc(int4& x, const int4& mask) {
	x -=mask;
}

inline void inc(int8& x, const int8& mask) {
	x -=mask;
}

inline void inc(long4& x, const long4& mask) {
	x -=mask;
}

inline void inc(long2& x, const long2& mask) {
	x -=mask;
}


inline void incf(float &x, const bool1f mask) {
    x++;
}

inline void incf(double &x, const bool1d mask) {
    x++;
}

inline void incf(float4& x, const bool4f& mask) {
    x += Vec4f(mask) & (Vec4f)1;
}

inline void incf(float8& x, const bool8f& mask) {
    x += Vec8f(mask) & (Vec8f)1;
}

inline void incf(double4& x, const bool4d& mask) {
    x += Vec4d(mask) & (Vec4d)1;
}

inline void incf(double2& x, const bool2d& mask) {
    x += Vec2d(mask) & (Vec2d)1;
}


inline float to_float_new(const int i) {
	return (float)i;
}

inline float4 to_float_new(const int4& i) {
	return to_float(i);
}

inline float8 to_float_new(const int8& i) {
	return to_float(i);
}

inline double4 to_float_new(const long4& i) {
	return to_double(i);
}

inline double2 to_float_new(const long2& i) {
	return to_double(i);
}

inline int truncate_to_int_new(const float x) {
	return (int)x;
}

inline int4 truncate_to_int_new(const float4& x) {
	return truncate_to_int(x);
}

inline int8 truncate_to_int_new(const float8& x) {
	return truncate_to_int(x);
}

inline long4 truncate_to_int_new(const double4& x) {
	return truncate_to_int64(x);
}

inline long2 truncate_to_int_new(const double2& x) {
	return truncate_to_int64(x);
}

template<int size>
int lookup(int1 index, int *x) {
	return x[index];
}

inline int8 get_mapedcolors(int8 index, int *x) {
	return lookup<2048>(index, x);
}

inline int4 get_mapedcolors(int4 index, int *x) {
	return lookup<2048>(index, x);
}

inline int1 get_mapedcolors(int1 index, int *x) {
	return lookup<2048>(index, x);
}

inline long4 get_mapedcolors(long4 index, int *x) {
	int4 index2 = compress(index, 0).get_low();
	//int4 index2 = compress(index, 0).get_low();
	int4 tmp = lookup<2048>(index2, x);
	long2 low = extend_low(tmp);
	long2 high = extend_high(tmp);
	long4 out(low, high);
	return out;
}

inline long2 get_mapedcolors(long2 index, int *x) {
	//int4 index2 = compress(index, 0);
	int x1 = x[index[0]];
	int x2 = x[index[1]];
	long2 out(x1,x2);
	return out;
}

inline long1 get_mapedcolors(long1 index, int *x) {
	return x[index];
}

/*
int64_t index loop_new(int64_t index, unsigned int *x) {
	return mColormap[index];

}
*/


inline void print_vec(const float8 &x) {
	for(int i=0; i<8; i++) {
		printf("%f\n", x[i]);
	}
	printf("\n");
}

inline void print_vec(const float4 &x) {
	for(int i=0; i<4; i++) {
		printf("%f\n", x[i]);
	}
	printf("\n");
}

inline void print_vec(const double4 &x) {
	for(int i=0; i<4; i++) {
		printf("%f\n", x[i]);
	}
	printf("\n");
}
#endif
